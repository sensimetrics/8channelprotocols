(function () {
    // utility function from example protocols
    const getCurrentN = function (responses, currentId) {
        var currentN = 0;               // initialize counter
        var index = responses.length - 1; // start index at most recent response

        // Loop through responses, starting with most recent, until another section (pageId) is found
        while (index >= 0 && (responses[index].presentationId.indexOf(currentId) > -1)) {
            currentN++; // add 1 to counter
            index--;    // decrease index to previous response
        }
        return currentN;
    };

    // timing
    var startLoadingFiles = 0;
    var startMixing = 0;
    var startPlaying = 0;
    var startDeleting = 0;
    var doneDeleting = 0;

    // async read functions

    // new functionality, not it demo app, no UI
    const readResolveMultipleURLs = function (paths, results, finalResolve, fail) {
        if (paths === undefined || paths === null) {
            fail("paths is undefined or null");
        } else if (paths) {
            startLoadingFiles = new Date();
            if (Array.isArray(paths)) {
                if (paths.length === 1) {
                    readResolveURL(paths[0], function (decoded) {
                        results.unshift(decoded);
                        // alert("paths[0]: " + paths[0] + "\n" +
                        //     JSON.stringify(Object.keys(decoded)) + "\n" +
                        //     "results.length: " + results.length
                        // );
                        finalResolve(results);
                    });
                } else {
                    const aPath = paths.pop();
                    readResolveURL(aPath, function (decoded) {
                        results.unshift(decoded);
                        // alert("aPath: " + aPath + "\n" +
                        //     JSON.stringify(Object.keys(decoded)) + "\n" +
                        //     "results.length: " + results.length
                        // );
                        readResolveMultipleURLs(paths, results, finalResolve, fail);
                    });
                }
            } else {
                fail("paths is not Array");
            }
        } else {
            fail("paths is 'not truthy'");
        }
    };

    const readResolveURL = function (path, resolve) {
        var decoded = {};

        resolveURL(path, function (fe) {
            readFileEntry(fe, function (data) {
                decoded = decodeWAV(data);

                // alert('decoded.sampleRate: ' + decoded.sampleRate);
                // alert('decoded.bitDepth: ' + decoded.bitDepth);
                // alert('decoded.channels dimensions: ' + '[' + decoded.channels.length + ',' + decoded.channels[0].length + ']');
                // alert('decoded.length: ' + decoded.length);

                resolve(decoded);
            });
        });
    };

    const resolveURL = function (url, resolve) {
        window.resolveLocalFileSystemURL(url, function (fe) {
            resolve(fe);
        }, function (err) {
            alert("err: " + JSON.stringify(err));
        });
    };


    const readFileEntry = function (fe, resolve) {
        var reader = new FileReader();
        reader.onloadend = function (evt) {
            resolve(evt.target.result);
        };

        fe.file(function (file) {
            reader.readAsArrayBuffer(file);
        });
    };

    // based on https://developers.google.com/web/updates/2012/06/How-to-convert-ArrayBuffer-to-and-from-String

    const ab2str = function (buf) {
        return String.fromCharCode.apply(null, new Uint8Array(buf));
    };

    // based on https://github.com/oampo/audiofile.js/blob/master/audiofile.js
    // wonderful code

    const readString = function (data, offset, length) {
        return ab2str(data.slice(offset, offset + length));
    };

    const readIntL = function (data, offset, length) {
        var buf = new Uint8Array(data);
        var value = 0;
        var i = 0;

        for (i = 0; i < length; i += 1) {
            // value = value + ((data.charCodeAt(offset + i) & 0xFF) *
            //                 Math.pow(2, 8 * i));
            value = value + (buf[offset + i] * Math.pow(2, 8 * i));
        }
        return value;
    };

    const readChunkHeaderL = function (data, offset) {
        var chunk = {};
        chunk.name = readString(data, offset, 4);
        chunk.length = readIntL(data, offset + 4, 4);
        return chunk;
    };

    const decodeWAV = function (data) {
        var fileLength;
        var wave;

        var encoding;
        var numberOfChannels;
        var sampleRate;
        var bitDepth;
        var bytesPerSample;

        var length;
        var channels;

        var decoded = {};
        var offset = 0;
        // Header
        var chunk = readChunkHeaderL(data, offset);
        offset += 8;
        if (chunk.name !== "RIFF") {
            alert("File is not a WAV");
            return null;
        }

        fileLength = chunk.length;
        fileLength += 8;

        wave = readString(data, offset, 4);
        offset += 4;
        if (wave !== "WAVE") {
            alert("File is not a WAV");
            return null;
        }

        while (offset < fileLength) {
            chunk = readChunkHeaderL(data, offset);
            offset += 8;
            if (chunk.name === "fmt ") {
                // File encoding
                encoding = readIntL(data, offset, 2);
                offset += 2;

                if (encoding !== 0x0001) {
                    // Only support PCM
                    alert("Cannot decode non-PCM encoded WAV file");
                    return null;
                }

                // Number of channels
                numberOfChannels = readIntL(data, offset, 2);
                offset += 2;

                // Sample rate
                sampleRate = readIntL(data, offset, 4);
                offset += 4;

                // Ignore bytes/sec - 4 bytes
                offset += 4;

                // Ignore block align - 2 bytes
                offset += 2;

                // Bit depth
                bitDepth = readIntL(data, offset, 2);
                bytesPerSample = bitDepth / 8;
                offset += 2;
            } else if (chunk.name === "data") {
                // Data must come after fmt, so we are okay to use it's variables
                // here
                length = chunk.length / (bytesPerSample * numberOfChannels);
                channels = [];
                let i;

                for (i = 0; i < numberOfChannels; i += 1) {
                    channels.push(new Float32Array(length));
                }

                for (i = 0; i < numberOfChannels; i += 1) {
                    const channel = channels[i];
                    let j;
                    for (j = 0; j < length; j += 1) {
                        let index = offset;
                        index += (j * numberOfChannels + i) * bytesPerSample;
                        // Sample
                        let value = readIntL(data, index, bytesPerSample);
                        // Scale range from 0 to 2**bitDepth -> -2**(bitDepth-1) to
                        // 2**(bitDepth-1)
                        let range = 1 << bitDepth - 1;
                        if (value >= range) {
                            value |= ~(range - 1);
                        }
                        // Scale range to -1 to 1
                        channel[j] = value / range;
                    }
                }
                offset += chunk.length;
            } else {
                offset += chunk.length;
            }
        }
        decoded.sampleRate = sampleRate;
        decoded.bitDepth = bitDepth;
        decoded.channels = channels;
        decoded.length = length;
        return decoded;
    };

    // audio processing

    const combineSelected = function (channelsOut, dcs, channelMaps) {
        // alert("dcs.length: " + dcs.length)
        // alert("channelMaps: " + JSON.stringify(channelMaps))
        const maxLength = dcs.reduce(function (largest, dc) {          //reduce the max for all dc
            return (largest > dc.channels[0].length ? largest : dc.channels[0].length);
        }, 0);

        // this only knows how to produce 8-channel outputs
        if (channelsOut !== 8) {
            alert("can only handle 8 output channes, not " + channelsOut);
            return null;
        }

        // and # of decoded files  must match # of maps
        if (dcs.length !== channelMaps.length) {
            alert("internal error - number of files != number of channel maps");
            return null;
        }

        // mix
        let mixed = [
            new Float32Array(maxLength),
            new Float32Array(maxLength),
            new Float32Array(maxLength),
            new Float32Array(maxLength),
            new Float32Array(maxLength),
            new Float32Array(maxLength),
            new Float32Array(maxLength),
            new Float32Array(maxLength)
        ];

        // alert("dcs.length: " + dcs.length)
        dcs.forEach(function (dc, dcIndex) {
            let channelMap = channelMaps[dcIndex];
            channelMap.forEach(function (outputInfo, inputChannel) {
                if (Array.isArray(outputInfo)) {
                    // loop over each element of outputInfo
                    outputInfo.forEach(function (oi) {
                        // alert('oi: ' + JSON.stringify(oi));
                        mixChannelData(oi, dc, inputChannel, mixed);
                    });
                } else {
                    // alert('outputInfo: ' + JSON.stringify(outputInfo));
                    mixChannelData(outputInfo, dc, inputChannel, mixed);
                }
            });
        });

        return mixed;
    };

    const mixChannelData = function (outputInfo, dc, inputChannel, mixed) {
        // get rid of ||-, &&-expressions too complicated to read
        if (outputInfo === null) {
            return;
        }

        const outputChannel = outputInfo["outputChannel"];
        if (outputChannel === "None") {
            // no output channel selected
            return;
        }
        if (isNaN(outputChannel)) {
            // some other error
            return;
        }
        if ((outputChannel < 0) || (outputChannel > 7)) {
            // not a valid output channel for 8-channel file
            return;
        }

        const channelData = dc.channels[inputChannel];
        addToMix(mixed, +outputChannel, channelData, +outputInfo["outputGain"]);
    };

    const addToMix = function (mixed, outputChannel, channelData, channelGain = 1.0) {
        channelData.forEach(function (sample, index) {
            mixed[outputChannel][index] += (sample * channelGain);
        })
    };

    // async play
    const deleteTmpAudio = function (filename, resolve, reject) {
        window.resolveLocalFileSystemURL('file:///storage/emulated/0/' + filename, function (fe) {
            fe.remove(resolve, reject);
        })
    };

    const playTmpAudio = function (filename, resolve, reject) {
        let media = new window.Media('file:///storage/emulated/0/' + filename,
            function () {
                media.release();
                // alert("media played");
                resolve(filename);
            }, function (err) {
                alert("media err: " + JSON.stringify(err));
                reject(filename);
            });

        media.play();
    };


    // async write
    const makeRandomName = function (n) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < n; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    };

    const writeWAV = function (fileName, data, resolve, reject) {
        let blob = encodeWAV(48000, data);
        // alert("encoded");

        resolveURLAndWrite('file:///storage/emulated/0/' /*hardcodedPathURL*/, fileName, blob, function (result) {
            resolve(fileName);
        }, function (err) {
            alert('resolveURLAndWrite failed: ' + err);
            reject(err);
        });
    };

    const resolveURLAndWrite = function (dirURL, fileName, fileBlob, outerResolve, outerFail) {
        window.resolveLocalFileSystemURL(dirURL, function (dirEntry) {
            dirEntry.getFile(fileName, { create: true }, function (file) {
                file.createWriter(function (fileWriter) {
                    fileWriter.onwriteend = function (event) {
                        outerResolve(event)
                    }
                    fileWriter.write(fileBlob)
                })
            })
        }, function (err) {
            alert('err: ' + err)
            outerFail(err)
        });
    }

    // convert audio into blob for writing

    // based on https://github.com/higuma/wav-audio-encoder-js/blob/master/lib/WavAudioEncoder.js
    // and https://stackoverflow.com/questions/13814621/how-can-i-get-the-dimensions-of-a-multidimensional-javascript-array
    const array_equals = function (a, b) {
        return a.length === b.length && a.every(function (value, index) {
            return value === b[index];
        });
    };

    const getdim = function (arr) {
        if (/*!(arr instanceof Array) || */!arr.length) {
            return []; // current array has no dimension
        }
        var dim = arr.reduce(function (result, current) {
            // check each element of arr against the first element
            // to make sure it has the same dimensions
            return array_equals(result, getdim(current)) ? result : false;
        }, getdim(arr[0]));

        // dim is either false or an array
        return dim && [arr.length].concat(dim);
    };

    const setString = function (view, offset, str) {
        let len = str.length;
        for (var i = 0; i < len; ++i) {
            view.setUint8(offset + i, str.charCodeAt(i))
        }
    };

    // convert data all at once - no collecting chunks
    const encodeWAV = function (sampleRate, data) {
        // expect max 2-D data
        let dims = getdim(data);
        if ((dims === []) || (dims.length <= 0) || (dims.length > 2) || (dims === false)) {
            alert('invalid dimensions in data');
            return null;
        }

        // if 2-D, number of samples same
        let numChannels = data.length;
        let numSamples = (numChannels == 1) ? dims[0] : dims[1];
        // alert("numChannels: " + numChannels + "\n" + "numSamples: " + numSamples);
        let dataView = new DataView(new ArrayBuffer(numSamples * numChannels * 2));
        var offset = 0;

        for (var i = 0; i < numSamples; ++i) {
            for (var ch = 0; ch < numChannels; ++ch) {
                // we presume Float32Array - scale to 16-bit integer
                var sample = (numChannels === 1) ? data[i] : data[ch][i];
                sample *= 0x7fff;
                sample = (sample < 0) ? Math.max(sample, -0x8000) : Math.min(sample, 0x7fff);
                dataView.setInt16(offset, sample, true);
                offset += 2;
            }
        }

        // construct header ...

        let dataSize = numChannels * (numSamples * 2);
        let headerView = new DataView(new ArrayBuffer(44));

        setString(headerView, 0, "RIFF");
        headerView.setUint32(4, 36 + dataSize, true);
        setString(headerView, 8, "WAVE");
        setString(headerView, 12, "fmt ");
        headerView.setUint32(16, 16, true);
        headerView.setUint16(20, 1, true);
        headerView.setUint16(22, numChannels, true);
        headerView.setUint32(24, sampleRate, true);
        headerView.setUint32(28, sampleRate * 4, true);
        headerView.setUint16(32, numChannels * 2, true);
        headerView.setUint16(34, 16, true);
        setString(headerView, 36, "data");
        headerView.setUint32(40, dataSize, true);

        // put samples & data out
        let dataViews = [];
        dataViews.push(dataView);
        dataViews.unshift(headerView);

        let blob = new Blob(dataViews, { type: "audio/wav" });
        return blob;
    }

    // from https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
    /**
     * Returns a random number between min (inclusive) and max (exclusive)
     */
    const getRandomArbitrary = function (min, max) {
        return Math.random() * (max - min) + min;
    };

    /**
     * Returns a random integer between min (inclusive) and max (inclusive).
     * The value is no lower than min (or the next integer greater than min
     * if min isn't an integer) and no greater than max (or the next integer
     * lower than max if max isn't an integer).
     * Using Math.round() will give you a non-uniform distribution!
     */
    const getRandomInt = function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };


    tabsint.register("injectedFn", function (dm) {
        var returnObject = {};
        // var bits = [91, 56, 54, 52, 48, 48, 58, 63, 93];
        // var stringFromBits = readString(bits, 0, 4);
        // alert("stringFromBits: " + stringFromBits);

        // resolveURL('file:///storage/emulated/0/' + 'Make8Long.wav',
        //     function (fileEntry) {
        //         alert("resolved: " + fileEntry.fullPath);
        //     });

        // readResolveURL('file:///storage/emulated/0/' + 'Make8Long.wav',
        //     // function (decoded) {
        //     //     alert("decoded.sampleRate: " + decoded.sampleRate + "\n" +
        //     //         "decoded.bitDepth: " + decoded.bitDepth + "\n" +
        //     //         "decoded.channels.length: " + decoded.channels.length + "\n" +
        //     //         "decoded.length: " + decoded.length);
        //     // });
        //     function(decoded) {
        //         alert("decoded.sampleRate: " + decoded.sampleRate);
        //     });
        // return returnObject;

        // readResolveMultipleURLs(['file:///storage/emulated/0/' + 'Make8Noise.wav'], [], function(results) {
        //     const keys = Object.keys(results[0]);
        //     alert("success: " + "\n" +
        //     "results.length: " + results.length + "\n" +
        //     "results[0] fields: " + JSON.stringify(keys)
        //     );
        // }, function(e) {
        //     alert("error: " + JSON.stringify(e));
        // });

        // readResolveMultipleURLs(['file:///storage/emulated/0/Make8Long.wav', 'file:///storage/emulated/0/Make8Noise.wav'],
        //     [], function (results) {
        //         const keys0 = Object.keys(results[0]);
        //         const keys1 = Object.keys(results[1]);
        //         alert("success: " + "\n" +
        //             "results.length: " + results.length + "\n" +
        //             "results[0] fields: " + JSON.stringify(keys0) + "\N" +
        //             "results[1] fields: " + JSON.stringify(keys1)
        //         );
        //     }, function (e) {
        //         alert("error: " + JSON.stringify(e));
        //     });


        // reads 2 files, plays 1 - no mixing yet
        // readResolveMultipleURLs(
        //     ['file:///storage/emulated/0/' + 'Make8Long.wav', 'file:///storage/emulated/0/' + 'Make8Noise.wav'],
        //     [], function (finalResults) {
        //         alert("success: - finalResults.length: " + finalResults.length);
        //         let i = 0;

        //         for (i = 0; i < finalResults.length; i += 1) {
        //             let t = Object.keys(finalResults[i]);
        //             alert("finalResults[" + i + "]: " + JSON.stringify(t));
        //         }

        //         let media = new window.Media('file:///storage/emulated/0/' + 'Make8Long.wav',
        //             function () {
        //                 media.release();
        //                 alert("media played");
        //             }, function (err) {
        //                 alert("media err: " + JSON.stringify(err));
        //             });

        //         media.play();
        //     }, function (e) {
        //         alert("error: " + JSON.stringify(e));
        // });

        // find presentation number for current section (by counting matching pageId's)
        var currentN = getCurrentN(dm.examResults.testResults.responses, dm.page.id);

        // generate random input channels, output channels, gains

        const inputToneIndex = getRandomInt(0, 7);
        const inputNoiseIndex = getRandomInt(0, 7);
        const outputToneIndex = getRandomInt(0, 7);
        const outputNoiseIndex = getRandomInt(0, 7);
        const toneGain = getRandomArbitrary(0.025, 0.25);
        const noiseGain = getRandomArbitrary(0.025, 0.25);

        // read 2 files, perform mix
        const fileNames = ['file:///storage/emulated/0/' + 'Make8Long.wav', 'file:///storage/emulated/0/' + 'Make8Noise.wav'];
        readResolveMultipleURLs(fileNames, [], function (finalResults) {
            // alert("success: - finalResults.length: " + finalResults.length);
            startMixing = new Date();

            const channelMaps = [
                [
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }]
                ],
                [
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }],
                    [{ "outputChannel": "None", "outputGain": "0" }]
                ]
            ];
            channelMaps[0][inputToneIndex][0].outputChannel = outputToneIndex;
            channelMaps[0][inputToneIndex][0].outputGain = toneGain;
            channelMaps[1][inputNoiseIndex][0].outputChannel = outputNoiseIndex;
            channelMaps[1][inputNoiseIndex][0].outputGain = noiseGain;

            // alert("channelMaps: " + JSON.stringify(channelMaps));

            let mixed = combineSelected(8, finalResults, channelMaps);

            // alert("mixed-\n" +
            //     "isArray: " + Array.isArray(mixed) + "\n" +
            //     "length: " + mixed.length + "\n" +
            //     "[0]'length:" + mixed[0].length
            // );

            let outputName = 'tmp' + makeRandomName(5) + '.wav'

            writeWAV(outputName, mixed, function () {
                // alert("should have written " + outputName);

                startPlaying = new Date();
                playTmpAudio(outputName, function (filename) {
                    startDeleting = new Date();
                    deleteTmpAudio(filename, function (status) {
                        doneDeleting = new Date();

                        // attempt to time operations
                        returnObject["startLoadingFiles"] = startLoadingFiles;
                        returnObject["startMixing"] = startMixing;
                        returnObject["startPlaying"] = startPlaying;
                        returnObject["startDeleting"] = startDeleting;
                        returnObject["doneDeleting"] = doneDeleting;
                        returnObject["totalTime"] = doneDeleting - startLoadingFiles;

                        // alert("removed status: " + status);
                    }, function (err) {
                        alert("failed to remove " + outputName + "\n" +
                            "err: " + JSON.stringify(err));
                    });
                }, function (filename) {
                    alert("failed to play " + filename);
                });
            }, function (err) {
                alert("failed to write " + outputName + ":" + JSON.stringify(err));
            });

        }, function (e) {
            alert("error: " + JSON.stringify(e));
        });

        returnObject["inputToneIndex"] = inputToneIndex;
        returnObject["inputNoiseIndex"] = inputNoiseIndex;
        returnObject["outputToneIndex"] = outputToneIndex;
        returnObject["outputNoiseIndex"] = outputNoiseIndex;
        returnObject["toneGain"] = toneGain;
        returnObject["noiseGain"] = noiseGain;

        return returnObject;
    });

})();